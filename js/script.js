const ourPhotos = document.querySelectorAll("#slider img");
const stopButton = document.querySelector(".isActiveButton");
const startButton = document.querySelector(".isInactiveButton");
let index = 0;

startButton.addEventListener("click",() => {
    clearInterval(slider);

     slider = setInterval(nextPhoto, 3000);
});
stopButton.addEventListener("click", () => {
    clearInterval(slider);

})
function nextPhoto () {

    if (index === ourPhotos.length - 1) {
        ourPhotos[index].classList.remove("ourSlide");
        ourPhotos[index].classList.add("ourImg");
        index = 0;
        ourPhotos[0].classList.remove("ourImg");
        ourPhotos[0].classList.add("ourSlide");
    }else {
        ourPhotos[index].classList.remove("ourSlide");
        ourPhotos[index].classList.add("ourImg");
        ourPhotos[index + 1].classList.remove("ourImg");
        ourPhotos[index + 1].classList.add("ourSlide");
        index++;
    }
}

let slider = setInterval(nextPhoto, 3000);
